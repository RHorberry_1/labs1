from J6305 import Spectrometer
import time
import numpy as np 
spec=Spectrometer()

Calib = input("Put in your calibration sample and write 'yes' ")
if Calib =='yes':
    spec.calibrate()
    time.sleep(1)
Readings = []
while True:
    WL = input('Enter your wavelength or q to quit ')
    if WL !='q':
        NWL = int(WL)
        spec.set_wavelength(NWL)
        time.sleep(1)
        Readings.append(spec.absorbance())
    else:
        print(Readings)
        break

y,x = [[z[i]for z in Readings] for i in (0,1)]

import matplotlib.pyplot as plt
plt.plot(x, y, 'r')
plt.ylabel('Absorbance (Abs)')
plt.xlabel('Wavelength (nm)')
plt.title('Wavelength vs Absorbance')
plt.show(block=False)
GI=input('Press enter to close Figure 1')
plt.close()
S = input('Would you like to save the graph? (yes/no) ')
if S =='yes':
    with open('Graph_Script_project.py', "r") as rf:
        with open(input('please input your save file name: '), "w") as wf:
            for line in rf:
                wf.write(line)

TR=[]
for p in y:
    TB = float(10**(2-p))
    TR.append(TB)
plt.plot(x, TR, 'x')
plt.xlabel('Wavelentgh (nm)')
plt.ylabel('Transmitance (%)')
plt.plot(np.unique(x), np.poly1d(np.polyfit(x,TR,1))(np.unique(x)), label='y=mx+c')
DD = np.polyfit(x,TR,1)
m = str(round(DD[0],2))
c= str(round(DD[1],2))
plt.title('Transmittance% vs Wavelentgh                y = ' + m + 'x + '+ c)
plt.show(block=False) 
GG = input('Press enter to close Figure 2')
plt.close()
S = input('Would you like to save the graph? (yes/no) ')
if S =='yes':
    with open('Graph_Script_project.py', "r") as rf:
        with open(input('please input your save file name: '), "w") as wf:
            for line in rf:
                wf.write(line)




