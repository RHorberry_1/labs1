import matplotlib.pyplot as plt
plt.plot([300,400,500,600], 'x')
plt.ylabel('Transmittance %')
plt.xlabel('wavelength(nm)')
plt.xlim(200,1000)
plt.ylim(0,100)
plt.title('wavelength vs transmittance')
plt.show()


