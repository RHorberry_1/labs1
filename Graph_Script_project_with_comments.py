from J6305 import Spectrometer #this is using the file with the code that runs the spectrometer 
import time # this allowed some of the functions to run more smoothly as it had taken time to run and complete the calibration 
import numpy as np #this is used to plot the best fit line and get the gradient with the y intecept  
import matplotlib.pyplot as plt #allows us to get all the functions for plotting graphs
spec=Spectrometer()

Calib = input("Put in your calibration sample and write 'yes' ") # asks the user to put their calibration sample in   
if Calib =='yes':
    spec.calibrate() # calibrating the spectrometer 
    time.sleep(1) #this was added to allow time for the spectrometer to calibrate before entering the wavelengths the user wanted 
Readings = [] # the empty list for the future readings from the spectrometer 
while True: #this loop allows the user to input as many wavelengths as they require and saves them to the list until they decide to quite
    WL = input('Enter your wavelength between a minimum of 198 and maximum of 1000 or q to quit ')#198 minimum and 1000 maximum the range has been given for the user to enter to prevent any errors from popping up making the experience easier  
    if WL !='q':
        NWL = int(WL) 
        spec.set_wavelength(NWL) # is telling the spectrometer to set the wavelength the user has entered 
        time.sleep(1) # this was added to allow time for the spectrometer to set to the new wavelength 
        Readings.append(spec.absorbance()) # the readings were being added to the list 
    else:
        print(Readings)
        break

y,x = [[z[i]for z in Readings] for i in (0,1)] # the readings from the spectrometer is saved as a list of tuples, this seperates the absorbance reading(y) from the wavelength readings(x) 

plt.plot(x, y, 'r') # this function plots the points on the graph as a line 
plt.ylabel('Absorbance (Abs)') #plt.xlabel and ylabel labels the axis with the appropriate given names
plt.xlabel('Wavelength (nm)')
plt.title('Wavelength vs Absorbance') #this was added to give the tittle to the graph
plt.show(block=False) # with the block=False argument allows the graph to be closed with a single key remotly 
GI=input('Press enter to close Figure 1') # this was added so the user can close the figure when they want after they have finished viewing it
plt.close() # this closes the graph after the key has been used
S = input('Would you like to save the graph? (yes/no) ') # this asks the user if they would like to save the graph for later viewing
if S =='yes':
    with open('Graph_Script_project.py', "r") as rf: #this reads the file that the user is looking for 
        with open(input('please input your save file name: '), "w") as wf: # this creates a new file with a new name given by the user through the input function
            for line in rf:# it specifies what is being written
                wf.write(line) # this writes into the file 

TR=[] # this is creating the empty list 
for p in y:
    TB = float(10**(2-p)) # this is the calculation for the transmittance(%) from the absorbance readings in the spectrometer 
    TR.append(TB) # adding results of the calculation(TB) to the earlier created list   
plt.plot(x, TR, 'x') # this function plots the points on the graph as x's
plt.xlabel('Wavelentgh (nm)') #plt.xlabel and ylabel labels the axis with the appropriate given names 
plt.ylabel('Transmitance (%)')
plt.plot(np.unique(x), np.poly1d(np.polyfit(x,TR,1))(np.unique(x))) # this is plotting the graph with the line of best fit 
DD = np.polyfit(x,TR,1) #this function allows for a best fit line to be plotted using the data entered e.g the wavelength and the data calculated the Transmittance(%). The first argument is the x value(x), the second is the y value(TR) and the third is the degree of polynomial(1) 
m = str(round(DD[0],2)) # the gradient(m) and the Y-Axis intercept (c) were taken from the fuction that makes the line of best fit(DD). The functions(m and c) had both been rounded to two decimal places for a more pleasing look
c= str(round(DD[1],2))
plt.title('Transmittance% vs Wavelentgh                y = ' + m + 'x + '+ c) #this was added next to the title so the user could easily get the gradient of the equation followed by the point at which it hits the Y~Axis 
plt.show(block=False) # with the block=False argument allows the graph to be closed with a single key remotly 
GG = input('Press enter to close Figure 2') # this was added so the user can close the figure when they want after they have finished viewing it 
plt.close() # this closes the graph after the key has been used 
S = input('Would you like to save the graph? (yes/no) ') # this asks the user if they would like to save the graph for later viewing 
if S =='yes':
    with open('Graph_Script_project.py', "r") as rf: #this reads the file that the user is looking for
        with open(input('please input your save file name: '), "w") as wf: # this creates a new file with a new name given by the user through the input function
            for line in rf:# it specifies what is being written
                wf.write(line) # this writes into the file 